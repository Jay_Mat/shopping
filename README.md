# APIs For Purchasing Products Online
Project For Different Kind Of APIs Related To Customer online Purchase

# Project build requirements
Java 11
Maven: Version 3.6.3 or later

# Running the Application 
                        ---------------- Development ------------------
1. `mvn install` builds the application and launches the application on default port 8080 

# Swagger end-point
http://localhost:8080/swagger-ui/

#Testing Purchase Endpoint
 `{
   "productsToPurchase": [
     "b2090194-1778-11eb-adc1-0242ac120001"
   ],
   "userName": "b2090194-1778-11eb-adc1-0242ac120001"
 }`