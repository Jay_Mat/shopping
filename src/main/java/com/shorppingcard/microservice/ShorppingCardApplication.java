package com.shorppingcard.microservice;

import com.shorppingcard.microservice.configuration.OrderProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@EnableSwagger2
@EnableConfigurationProperties(OrderProperties.class)
public class ShorppingCardApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShorppingCardApplication.class, args);
    }

}
