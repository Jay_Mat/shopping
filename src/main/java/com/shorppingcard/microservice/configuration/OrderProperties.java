package com.shorppingcard.microservice.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "order")
@Getter
@Setter
public class OrderProperties {

    private String timeInterval;

    private Integer premiumPoints;
}
