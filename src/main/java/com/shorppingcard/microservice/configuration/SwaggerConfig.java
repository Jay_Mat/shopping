package com.shorppingcard.microservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    ApiInfo apiInfo = new ApiInfo(
            "Beauty eMall",
            "Purchase Product Project Backend endpoints",
            "1.0",
            "https://www.purchaseproductsfromsouthafrica.co.za",
            new Contact("Product Dev Team", "https://www.purchaseproductsfromsouthafrica.co.za", "info@purchaseproductsfromsouthafrica.co.za"),
            "",
            "",
            new ArrayList<>());
    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

}
