package com.shorppingcard.microservice.constants;

public class MessagesConstants {

    public static final String PURCHASE_UNSUCCESSFUL = "User Purchase UnSuccessful,User has %d points, purchase cost %d";

    public static final String PURCHASE_SUCCESSFUL = "User Purchase Successful";
}
