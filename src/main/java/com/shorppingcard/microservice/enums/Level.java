package com.shorppingcard.microservice.enums;

import lombok.Getter;

@Getter
public enum Level {
    LEVEL_ONE("Level One"),
    LEVEL_TWO("Level Two"),
    LEVEL_THREE("Level Three");

    private final String rating;

    Level(String _rating) {
        this.rating = _rating;
    }
}
