package com.shorppingcard.microservice.enums;

public enum OrderStatus {
    PENDING,
    READY_FOR_SHIPPING,
    SHIPPED
}
