package com.shorppingcard.microservice.models.request;

import com.shorppingcard.microservice.entity.Product;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class ProductRequest {

    List<Product> products;
}
