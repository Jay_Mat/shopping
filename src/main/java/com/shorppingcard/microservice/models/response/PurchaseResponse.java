package com.shorppingcard.microservice.models.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponse {

    @Builder.Default
    private Integer currentPoints = 0;

    @Builder.Default
    private Integer pointsDeducted = 0;

    private Boolean purchaseSuccessful;

    private String message;

}
