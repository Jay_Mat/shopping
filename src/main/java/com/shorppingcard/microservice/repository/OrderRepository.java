package com.shorppingcard.microservice.repository;

import com.shorppingcard.microservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order,String> {
    List<Order> findAllByDeliveryDate(LocalDate date);
}
