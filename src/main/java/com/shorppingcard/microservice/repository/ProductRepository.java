package com.shorppingcard.microservice.repository;

import com.shorppingcard.microservice.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductRepository extends PagingAndSortingRepository<Product,String> {
    Page<Product> findAll(Pageable pageable);
}
