package com.shorppingcard.microservice.repository;

import com.shorppingcard.microservice.entity.Tier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TierRepository extends JpaRepository<Tier,String> {
}
