package com.shorppingcard.microservice.repository;

import com.shorppingcard.microservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
