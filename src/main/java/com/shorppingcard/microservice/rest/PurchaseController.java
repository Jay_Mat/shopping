package com.shorppingcard.microservice.rest;

import com.shorppingcard.microservice.models.request.PurchaseRequest;
import com.shorppingcard.microservice.models.response.PurchaseResponse;
import com.shorppingcard.microservice.services.PurchaseService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/api/shoppingCard/purchase")
public class PurchaseController {

    private final PurchaseService purchaseService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Make Purchase", notes = "API For Making Purchase")
    public ResponseEntity<PurchaseResponse> makePurchase(@RequestBody PurchaseRequest purchaseRequest) throws Exception {
        return ResponseEntity.ok(purchaseService.makePurchase(purchaseRequest));
    }
}
