package com.shorppingcard.microservice.services;

import com.shorppingcard.microservice.configuration.OrderProperties;
import com.shorppingcard.microservice.entity.Order;
import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.entity.User;
import com.shorppingcard.microservice.enums.OrderStatus;
import com.shorppingcard.microservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    private final OrderProperties orderProperties;

    public Order createOrderForUser(User user, List<Product> products) {
        return orderRepository.save(getOrderToSave(user,products));
    }

    private Order getOrderToSave(User user, List<Product> products){
        return Order.builder()
                .deliveryDate(getDeliveryDate(products))
                .productList(new HashSet<>(products))
                .status(OrderStatus.PENDING)
                .user(user).build();
    }

    private LocalDate getDeliveryDate(List<Product> products) {
        int totalCost = products.stream().mapToInt(Product::getPoints).sum();

        //5 days if user spend premium points, otherwise normal delivery time, 7 days.
        return totalCost >= orderProperties.getPremiumPoints() ? LocalDate.now().plusDays(5) : LocalDate.now().plusDays(7) ;
    }


    //updates orders daily which are ready for shipping the following day
    @Scheduled(fixedRateString="${order.timeInterval}")
    public void UpdateShippingOrders() {
        List<Order> orders = new ArrayList<>();

        orderRepository.findAllByDeliveryDate(LocalDate.now().plusDays(1)).forEach(order -> {
        order.setStatus(OrderStatus.READY_FOR_SHIPPING);
        orders.add(order);
        });

        orderRepository.saveAll(orders);
    }

    //updates orders already shipped
    @Scheduled(fixedRateString="${order.timeInterval}")
    public void UpdateShippedOrders() {
        List<Order> orders = new ArrayList<>();

        orderRepository.findAllByDeliveryDate(LocalDate.now()).forEach(order -> {
            order.setStatus(OrderStatus.SHIPPED);
            orders.add(order);
        });

        orderRepository.saveAll(orders);
    }

}
