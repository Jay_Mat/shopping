package com.shorppingcard.microservice.services;

import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public List<Product> findAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    public List<Product> findAllProductsByIds(List<String> products) {
        return (List<Product>) productRepository.findAllById(products);
    }

}
