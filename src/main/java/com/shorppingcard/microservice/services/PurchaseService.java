package com.shorppingcard.microservice.services;


import com.shorppingcard.microservice.constants.MessagesConstants;
import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.entity.Tier;
import com.shorppingcard.microservice.entity.User;
import com.shorppingcard.microservice.models.request.PurchaseRequest;
import com.shorppingcard.microservice.models.response.PurchaseResponse;
import com.shorppingcard.microservice.repository.TierRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class PurchaseService {

    private final UserService userService;

    private final ProductService productService;

    private final TierRepository tierRepository;

    private final OrderService orderService;

    public PurchaseResponse makePurchase(PurchaseRequest purchaseRequest) throws Exception {

        User user = userService.findUserById(purchaseRequest.getUserName());
        Integer currentPoints = user.getTier().getPoints();

        List<Product> productsToPurchase = productService.findAllProductsByIds(purchaseRequest.getProductsToPurchase());
        Integer totalCost = getProductsTotalCost(productsToPurchase);
        boolean isPurchasePossible = isPurchasePossible(user,totalCost);

        if (isPurchasePossible) {
            updateUserPoints(user, totalCost);
            orderService.createOrderForUser(user,productsToPurchase);
        }
        return isPurchasePossible? getPurchaseResponse(currentPoints,totalCost,true, MessagesConstants.PURCHASE_SUCCESSFUL) :
                getPurchaseResponse(currentPoints,totalCost,false,String.format(MessagesConstants.PURCHASE_UNSUCCESSFUL,currentPoints,totalCost)) ;

    }

    private boolean isPurchasePossible(User user, Integer totalCost){
     return user.getTier().getPoints() >= totalCost;
    }

    private Integer getProductsTotalCost(List<Product> productsToPurchase){
       return productsToPurchase.stream().collect(Collectors.summingInt(Product::getPoints));
    }

    private PurchaseResponse getPurchaseResponse(Integer currentPoints, Integer totalCost,boolean purchaseSuccessful,String message){
        return PurchaseResponse.builder()
                .currentPoints(currentPoints)
                .pointsDeducted(totalCost)
                .purchaseSuccessful(purchaseSuccessful)
                .message(message)
                .build();
    }

    private void updateUserPoints(User user, Integer totalCost){
        Tier tier = user.getTier();
        Integer currentPoints = tier.getPoints();
        tier.setPoints(currentPoints-totalCost);
        tierRepository.save(tier);
    }

}
