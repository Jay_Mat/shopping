package com.shorppingcard.microservice.services;


import com.shorppingcard.microservice.entity.User;
import com.shorppingcard.microservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User findUserById(String userId) throws Exception {
        return userRepository.findById(userId).orElseThrow(()->
                new Exception(String.format("User with id %s not Found",userId)));
    }
}
