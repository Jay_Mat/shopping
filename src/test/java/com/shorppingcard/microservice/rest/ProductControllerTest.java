package com.shorppingcard.microservice.rest;

import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest extends AbstractControllerTest{

    private static final String BASE_URI = "/v1/api/shoppingCard/products";

    @Autowired
    ProductRepository productRepository;

    @BeforeEach
    public void setup() throws Exception {
        setUpProductsTestData();
    }

    @Test
    public void shouldGetAllProducts() throws Exception {


        MvcResult mvcResult = getMockMvc().perform(MockMvcRequestBuilders
                .get(BASE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString()).isNotNull();
    }

   void setUpProductsTestData(){
       productRepository.save(Product.builder()
               .name("Product 1")
               .points(1).build());

       productRepository.save(Product.builder()
               .name("Product 2")
               .points(1).build());
    }

}
