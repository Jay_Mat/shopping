package com.shorppingcard.microservice.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shorppingcard.microservice.constants.MessagesConstants;
import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.entity.Tier;
import com.shorppingcard.microservice.entity.User;
import com.shorppingcard.microservice.models.request.PurchaseRequest;
import com.shorppingcard.microservice.repository.ProductRepository;
import com.shorppingcard.microservice.repository.TierRepository;
import com.shorppingcard.microservice.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PurchaseControllerTest extends AbstractControllerTest{

    private static final String BASE_URI = "/v1/api/shoppingCard/purchase";

    @Autowired
    UserRepository userRepository;

    @Autowired
    TierRepository tierRepository;

    @Autowired
    ProductRepository productRepository;

    User user;

    Product product;

    @BeforeEach
    public void setup() throws Exception {
        product = getProduct();
        user = getUser();
        setupTierTestData(user);
    }

    @Test
    public void shouldMakeSuccessfulPurchase() throws Exception {


        MvcResult mvcResult = getMockMvc().perform(MockMvcRequestBuilders
                .post(BASE_URI)
                .content(asJsonString(getPurchaseRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString()).contains(MessagesConstants.PURCHASE_SUCCESSFUL);
    }

    @Test
    public void shouldMakeUnsuccessfulPurchase() throws Exception {


        product.setPoints(4000); // update product to make it expensive (i.e costs more points than what user has)

        updateProduct(product);

        MvcResult mvcResult = getMockMvc().perform(MockMvcRequestBuilders
                .post(BASE_URI)
                .content(asJsonString(getPurchaseRequest()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();


        assertThat(mvcResult.getResponse().getContentAsString()).contains("User Purchase UnSuccessful");
    }


    User getUser(){
        return userRepository.save(User.builder().name("TestUser").build());
    }

    PurchaseRequest getPurchaseRequest(){
        return PurchaseRequest.builder().productsToPurchase(Collections.singletonList(product.getId())).userName(user.getId()).build();
    }

    void setupTierTestData(User user){
        tierRepository.save(Tier.builder().level("Level 1").points(345).user(user).build());
    }

    Product getProduct(){
    return     productRepository.save(Product.builder()
                .name("Product 1")
                .points(1).build());

    }

   void updateProduct(Product product){
        productRepository.save(product);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
