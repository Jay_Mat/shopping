package com.shorppingcard.microservice.services;

import com.shorppingcard.microservice.entity.Product;
import com.shorppingcard.microservice.entity.Tier;
import com.shorppingcard.microservice.entity.User;
import com.shorppingcard.microservice.models.request.PurchaseRequest;
import com.shorppingcard.microservice.repository.TierRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class PurchaseServiceTest {

    @Mock
    UserService userService;

    @Mock
    ProductService productService;

    @Mock
    TierRepository tierRepository;

    @InjectMocks
    private PurchaseService purchaseService;

    @Mock
    private OrderService orderService;

    @Test
    void shouldMakePurchase() throws Exception {
        when(userService.findUserById(anyString())).thenReturn(getUser());
        when(productService.findAllProducts()).thenReturn(getProducts());
        when(productService.findAllProductsByIds(anyList())).thenReturn(getProducts());

        purchaseService.makePurchase(PurchaseRequest.builder().userName("1").build());

        verify(userService, times(1)).findUserById(anyString());
        verify(userService, times(1)).findUserById(anyString());

    }


    User getUser() {
        Tier tier = Tier.builder().points(345).level("Level 1").build();
        return User.builder().name("TestUser").tier(tier).build();
    }

    List<Product> getProducts() {
        return Collections.singletonList(Product.builder().points(123).name("Product 1").build());
    }
}
